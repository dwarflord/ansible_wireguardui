# Wireguard VPN Server

Роль устанавливает сам wireguard и Web интерфейс для управления пользователями VPN

Wireguard UI конфигуррируется через переменные окружения.
Менять их значения можно пеоременными роли 

```
---
# wireguard settings
wireguard_bind_address: 0.0.0.0
wireguard_bind_port: 5000

# wireguard web ui settings
wireguard_wgui_systemuser: root
wireguard_disable_login: False
wireguard_session_secret: ''
wireguard_wgui_username: admin
wireguard_wgui_password: admin

wireguard_wgui_endpoint_address: 127.0.0.1

# wireguarg ui mail settings
wireguard_email_from_address: ''
wireguard_email_from_name: ''
wireguard_smtp_auth_type: ''
wireguard_smtp_no_tls_check: ''
wireguard_smtp_password: ''
wireguard_smtp_username: ''
wireguard_smtp_port: ''
wireguard_smtp_hostname: ''
wireguard_sendgrid_api_key: ''
```

Wireguard UI можно спрядать за реверс прокси nginx.
```
wireguard_enable_nginx: True
wireguard_bind_address: 127.0.0.1
wireguard_bind_port: 5000
```
Иначе UI будет доступен на прямую через порт 5000, либо который будет указан в переменной 
```
wireguard_bind_port
```

Для дополнительной защиты можно включить http basic auth в nginx
```
wireguard_auth_basic: True
```
Логин и пароль можно сгенерить через httpasswd. Файл с кредами находится в templates/nginx.htpasswd (default: admin/admin)

Домен по которому будет доустунеп web ui можно установить чепез переменную 
```
wireguard_domain
```

Подробнее про Wireguard UI можно посмотреть в оф. репозитории https://github.com/ngoduykhanh/wireguard-ui/

Пример playbook

inventory.yaml
```
---
all:
  hosts:
  vars:
    ansible_user: 'root'
    ssh_user: 'root'

vpn:
  hosts:
    vpn.example.com:
      ansible_host: $IP_YOU_SERVER
```
```
---
- hosts: vpn
  roles:
    - base
    - wireguard
  vars:
    wireguard_domain: "vpn.example.com"
    wireguard_wgui_password: "YOU ADMIN UI PASSWORD"

```