server {

    listen {{ wireguard_nginx_port }};
    server_name {{ wireguard_domain }};

    access_log /var/log/nginx/wg-access.log combined;
    error_log /var/log/nginx/wg-error.log;

    {% if wireguard_letsencrypt_enabled == True %}
    return 301 https://$host$request_uri;
    {% else %}


    location / {
        proxy_pass http://{{ wireguard_bind_address }}:{{ wireguard_bind_port }};
        proxy_buffering                    off;
        proxy_set_header Host              $http_host;
        proxy_set_header X-Real-IP         $remote_addr;
        proxy_set_header X-Forwarded-For   $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
    }
    {% endif %}
}

{% if wireguard_letsencrypt_enabled == True %}
server {

    listen {{ wireguard_nginx_ssl_port }} ssl;
    server_name {{ wireguard_domain }};

    ssl_certificate /etc/letsencrypt/live/{{ wireguard_domain }}/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/{{ wireguard_domain }}/privkey.pem;

    ssl_session_cache shared:le_nginx_SSL:2m;
    ssl_session_timeout 1440m;
    ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
    ssl_prefer_server_ciphers on;
    ssl_ciphers "ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA:ECDHE-RSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-RSA-AES256-SHA256:DHE-RSA-AES256-SHA:ECDHE-ECDSA-DES-CBC3-SHA:ECDHE-RSA-DES-CBC3-SHA:EDH-RSA-DES-CBC3-SHA:AES128-GCM-SHA256:AES256-GCM-SHA384:AES128-SHA256:AES256-SHA256:AES128-SHA:AES256-SHA:DES-CBC3-SHA:!DSS";

    access_log /var/log/nginx/wg_ssl-access.log combined;
    error_log /var/log/nginx/wg_ssl-error.log;

    location / {
        proxy_pass http://{{ wireguard_bind_address }}:{{ wireguard_bind_port }};
        proxy_buffering                    off;
        proxy_set_header Host              $http_host;
        proxy_set_header X-Real-IP         $remote_addr;
        proxy_set_header X-Forwarded-For   $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
    }
}
{% endif %}
